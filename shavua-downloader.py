from html.parser import HTMLParser
import http.cookiejar, urllib.request


# Download podcast, if found one.
class ShavuaDownloader(HTMLParser):
    def __init__(self):
        super().__init__()
        self.link = ''

    def _get_html(self, episodeNumber):
        url = "http://www.shavua.net/" + episodeNumber
        request = urllib.request.urlopen(url)
        return request.read().decode(request.headers.get_content_charset())

    def containes_class(self, attrs, name):
        return len(list(filter(lambda x: ((x[0] == 'class') and (x[1] == name)), attrs)))

    def download_podcast(self, episode):
        data = self._get_html(episode)
        super(ShavuaDownloader, self).feed(data)
        cj = http.cookiejar.CookieJar()
        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
        opener.addheaders = [('User-Agent',
                              'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36')]
        urllib.request.install_opener(opener)
        urllib.request.urlretrieve(self.link, episode + '.mp3')
        print('Finished to download episode number: %s' % episode)

    def handle_starttag(self, tag, attrs):
        if tag.startswith('a') and self.containes_class(attrs, 'podcast-episode-download'):
            self.link = [link[1] for link in attrs if link[0].startswith('href')][0]


# instantiate downloader instance
downloader = ShavuaDownloader()

for i in range(5, 319):
    downloader.download_podcast(str(i))
